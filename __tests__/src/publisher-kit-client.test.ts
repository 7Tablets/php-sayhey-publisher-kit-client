describe('Testing PublisherKitClient library', () => {
  const socketIO = {
    on: (event: string, cb: () => void) => {
      cb();
      return socketIO;
    },
    connect: jest.fn(),
    connected: true,
    disconnect: jest.fn(),
  };
  beforeEach(() => {
    jest.resetModules();
    socketIO.disconnect.mockClear();
    jest.doMock('socket.io-client', () => () => socketIO);
  });
  it('should return false when library kit is initializeialized incorrectly', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    expect(
      PublisherKitClient.initialize({
        appToken: '',
        clientApiKey: '',
        appId: '',
      }),
    ).toBe(false);
  });

  it('should return true when library kit is initializeialized correctly', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    PublisherKitClient._initialized = false;
    expect(
      PublisherKitClient.initialize({
        appToken: 'fasdf',
        clientApiKey: 'gsdfgsfg',
        appId: 'adfasf',
      }),
    ).toBe(true);
  });

  it('should return true when library is being initializeialized again', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    expect(
      PublisherKitClient.initialize({
        appToken: 'fasdf',
        clientApiKey: 'gsdfgsfg',
        appId: 'adfasf',
      }),
    ).toBe(true);
    expect(
      PublisherKitClient.initialize({
        appToken: 'fasdf',
        clientApiKey: 'gsdfgsfg',
        appId: 'adfasf',
      }),
    ).toBe(true);
  });

  it('should not allow user to connect if library is not initializeialized', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    PublisherKitClient.onConnect(() => {})
      .onMessage(() => {})
      .onDisconnect(() => {})
      .onError(() => {})
      .onReconnect(() => {});
    expect(PublisherKitClient.connect).toThrowError();
  });

  it('should not allow user to connect if library events are not subscribed to', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    expect(PublisherKitClient.connect).toThrowError();
  });

  it('should allow user to connect if library is initializeialized and events are subscribed to', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    PublisherKitClient.initialize({
      appToken: 'fasdf',
      clientApiKey: 'gsdfgsfg',
      appId: 'adfasf',
    });
    PublisherKitClient.onConnect(() => {}).onMessage(() => {});
    expect(PublisherKitClient.connect).not.toThrowError();
  });

  it('should disconnect socket when disconnect method is called', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    PublisherKitClient.initialize({
      appToken: 'fasdf',
      clientApiKey: 'gsdfgsfg',
      appId: 'adfasf',
    });
    PublisherKitClient.onConnect(() => {}).onMessage(() => {});
    PublisherKitClient.connect();
    PublisherKitClient.disconnect();
    expect(socketIO.disconnect).toHaveBeenCalledTimes(1);
  });

  it('should not call disconnect socket when disconnect method is called and socket is not yet connected', () => {
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    PublisherKitClient.initialize({
      appToken: 'fasdf',
      clientApiKey: 'gsdfgsfg',
      appId: 'adfasf',
    });
    PublisherKitClient.onConnect(() => {}).onMessage(() => {});
    PublisherKitClient.disconnect();
    expect(socketIO.disconnect).not.toHaveBeenCalled();
  });

  it('should call event listeners', () => {
    const onConnect = jest.fn();
    const onMessage = jest.fn();
    const onDisconnect = jest.fn();
    const onReconnect = jest.fn();
    const onError = jest.fn();
    const PublisherKitClient = require('../../src/publisher-kit-client').default;
    PublisherKitClient.initialize({
      appToken: 'fasdf',
      clientApiKey: 'gsdfgsfg',
      appId: 'adfasf',
    });
    PublisherKitClient.onConnect(onConnect)
      .onMessage(onMessage)
      .onDisconnect(onDisconnect)
      .onError(onError)
      .onReconnect(onReconnect);
    PublisherKitClient.connect();
    expect(onConnect).toHaveBeenCalledTimes(1);
    expect(onMessage).toHaveBeenCalledTimes(1);
    expect(onError).toHaveBeenCalledTimes(1);
    expect(onReconnect).toHaveBeenCalledTimes(1);
    expect(onDisconnect).toHaveBeenCalledTimes(1);
  });
});
