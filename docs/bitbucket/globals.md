[php-sayhey-publisher-kit-client - v1.0.0](README.md) › [Globals](globals.md)

# php-sayhey-publisher-kit-client - v1.0.0

## Index

### Classes

* [PublisherKitClient](classes/publisherkitclient.md)

### Interfaces

* [EventMessage](interfaces/eventmessage.md)
* [InitOptions](interfaces/initoptions.md)
