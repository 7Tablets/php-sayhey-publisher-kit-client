[php-sayhey-publisher-kit-client - v1.0.0](../README.md) › [Globals](../globals.md) › [EventMessage](eventmessage.md)

# Interface: EventMessage

## Hierarchy

* **EventMessage**

## Index

### Properties

* [event](eventmessage.md#markdown-header-event)
* [payload](eventmessage.md#markdown-header-payload)

## Properties

###  event

• **event**: *string*

Defined in types.ts:10

___

###  payload

• **payload**: *any*

Defined in types.ts:11
