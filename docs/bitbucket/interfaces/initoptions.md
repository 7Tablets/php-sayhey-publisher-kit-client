[php-sayhey-publisher-kit-client - v1.0.0](../README.md) › [Globals](../globals.md) › [InitOptions](initoptions.md)

# Interface: InitOptions

## Hierarchy

* **InitOptions**

## Index

### Properties

* [appId](initoptions.md#markdown-header-appid)
* [appToken](initoptions.md#markdown-header-optional-apptoken)
* [clientApiKey](initoptions.md#markdown-header-clientapikey)
* [publisherUrl](initoptions.md#markdown-header-optional-publisherurl)
* [userId](initoptions.md#markdown-header-optional-userid)

## Properties

###  appId

• **appId**: *string*

Defined in types.ts:3

___

### `Optional` appToken

• **appToken**? : *undefined | string*

Defined in types.ts:2

___

###  clientApiKey

• **clientApiKey**: *string*

Defined in types.ts:4

___

### `Optional` publisherUrl

• **publisherUrl**? : *undefined | string*

Defined in types.ts:5

___

### `Optional` userId

• **userId**? : *undefined | string*

Defined in types.ts:6
