[php-sayhey-publisher-kit-client - v1.0.0](../README.md) › [Globals](../globals.md) › [PublisherKitClient](publisherkitclient.md)

# Class: PublisherKitClient

## Hierarchy

* **PublisherKitClient**

## Index

### Properties

* [_appId](publisherkitclient.md#markdown-header-static-_appid)
* [_appToken](publisherkitclient.md#markdown-header-static-_apptoken)
* [_clientApiKey](publisherkitclient.md#markdown-header-static-_clientapikey)
* [_connectCallback](publisherkitclient.md#markdown-header-static-optional-_connectcallback)
* [_disconnectCallback](publisherkitclient.md#markdown-header-static-optional-_disconnectcallback)
* [_errorCallback](publisherkitclient.md#markdown-header-static-optional-_errorcallback)
* [_initialized](publisherkitclient.md#markdown-header-static-_initialized)
* [_isConnected](publisherkitclient.md#markdown-header-static-_isconnected)
* [_messageCallback](publisherkitclient.md#markdown-header-static-_messagecallback)
* [_platform](publisherkitclient.md#markdown-header-static-_platform)
* [_publisherUrl](publisherkitclient.md#markdown-header-static-_publisherurl)
* [_reconnectCallback](publisherkitclient.md#markdown-header-static-optional-_reconnectcallback)
* [_socket](publisherkitclient.md#markdown-header-static-_socket)
* [_subscribed](publisherkitclient.md#markdown-header-static-_subscribed)
* [_userId](publisherkitclient.md#markdown-header-static-_userid)

### Methods

* [_socketUrl](publisherkitclient.md#markdown-header-static-_socketurl)
* [connect](publisherkitclient.md#markdown-header-static-connect)
* [disconnect](publisherkitclient.md#markdown-header-static-disconnect)
* [initialize](publisherkitclient.md#markdown-header-static-initialize)
* [isConnected](publisherkitclient.md#markdown-header-static-isconnected)
* [onConnect](publisherkitclient.md#markdown-header-static-onconnect)
* [onDisconnect](publisherkitclient.md#markdown-header-static-ondisconnect)
* [onError](publisherkitclient.md#markdown-header-static-onerror)
* [onMessage](publisherkitclient.md#markdown-header-static-onmessage)
* [onReconnect](publisherkitclient.md#markdown-header-static-onreconnect)
* [updateAccessParams](publisherkitclient.md#markdown-header-static-updateaccessparams)

## Properties

### `Static` _appId

▪ **_appId**: *string*

Defined in publisher-kit-client.ts:12

___

### `Static` _appToken

▪ **_appToken**: *string*

Defined in publisher-kit-client.ts:9

___

### `Static` _clientApiKey

▪ **_clientApiKey**: *string*

Defined in publisher-kit-client.ts:15

___

### `Static` `Optional` _connectCallback

▪ **_connectCallback**? : *undefined | function*

Defined in publisher-kit-client.ts:24

___

### `Static` `Optional` _disconnectCallback

▪ **_disconnectCallback**? : *undefined | function*

Defined in publisher-kit-client.ts:27

___

### `Static` `Optional` _errorCallback

▪ **_errorCallback**? : *undefined | function*

Defined in publisher-kit-client.ts:30

___

### `Static` _initialized

▪ **_initialized**: *boolean* = false

Defined in publisher-kit-client.ts:38

___

### `Static` _isConnected

▪ **_isConnected**: *boolean* = false

Defined in publisher-kit-client.ts:42

___

### `Static` _messageCallback

▪ **_messageCallback**: *function*

Defined in publisher-kit-client.ts:36

#### Type declaration:

▸ (`message`: [EventMessage](../interfaces/eventmessage.md)): *void*

**Parameters:**

Name | Type |
------ | ------ |
`message` | [EventMessage](../interfaces/eventmessage.md) |

___

### `Static` _platform

▪ **_platform**: *"web" | "ios" | "android"*

Defined in publisher-kit-client.ts:46

___

### `Static` _publisherUrl

▪ **_publisherUrl**: *string* = "http://localhost:3001"

Defined in publisher-kit-client.ts:18

___

### `Static` `Optional` _reconnectCallback

▪ **_reconnectCallback**? : *undefined | function*

Defined in publisher-kit-client.ts:33

___

### `Static` _socket

▪ **_socket**: *Socket*

Defined in publisher-kit-client.ts:21

___

### `Static` _subscribed

▪ **_subscribed**: *boolean* = false

Defined in publisher-kit-client.ts:40

___

### `Static` _userId

▪ **_userId**: *string*

Defined in publisher-kit-client.ts:44

## Methods

### `Static` _socketUrl

▸ **_socketUrl**(): *string*

Defined in publisher-kit-client.ts:49

**Returns:** *string*

___

### `Static` connect

▸ **connect**(): *void*

Defined in publisher-kit-client.ts:79

**Returns:** *void*

___

### `Static` disconnect

▸ **disconnect**(): *void*

Defined in publisher-kit-client.ts:122

**Returns:** *void*

___

### `Static` initialize

▸ **initialize**(`options`: [InitOptions](../interfaces/initoptions.md)): *boolean*

Defined in publisher-kit-client.ts:53

**Parameters:**

Name | Type |
------ | ------ |
`options` | [InitOptions](../interfaces/initoptions.md) |

**Returns:** *boolean*

___

### `Static` isConnected

▸ **isConnected**(): *boolean*

Defined in publisher-kit-client.ts:120

**Returns:** *boolean*

___

### `Static` onConnect

▸ **onConnect**(`cb`: function): *[PublisherKitClient](publisherkitclient.md)*

Defined in publisher-kit-client.ts:128

**Parameters:**

▪ **cb**: *function*

▸ (): *void*

**Returns:** *[PublisherKitClient](publisherkitclient.md)*

___

### `Static` onDisconnect

▸ **onDisconnect**(`cb`: function): *[PublisherKitClient](publisherkitclient.md)*

Defined in publisher-kit-client.ts:133

**Parameters:**

▪ **cb**: *function*

▸ (`reason`: string): *void*

**Parameters:**

Name | Type |
------ | ------ |
`reason` | string |

**Returns:** *[PublisherKitClient](publisherkitclient.md)*

___

### `Static` onError

▸ **onError**(`cb`: function): *[PublisherKitClient](publisherkitclient.md)*

Defined in publisher-kit-client.ts:138

**Parameters:**

▪ **cb**: *function*

▸ (`e`: Error): *void*

**Parameters:**

Name | Type |
------ | ------ |
`e` | Error |

**Returns:** *[PublisherKitClient](publisherkitclient.md)*

___

### `Static` onMessage

▸ **onMessage**(`cb`: function): *[PublisherKitClient](publisherkitclient.md)*

Defined in publisher-kit-client.ts:148

**Parameters:**

▪ **cb**: *function*

▸ (`message`: [EventMessage](../interfaces/eventmessage.md)): *void*

**Parameters:**

Name | Type |
------ | ------ |
`message` | [EventMessage](../interfaces/eventmessage.md) |

**Returns:** *[PublisherKitClient](publisherkitclient.md)*

___

### `Static` onReconnect

▸ **onReconnect**(`cb`: function): *[PublisherKitClient](publisherkitclient.md)*

Defined in publisher-kit-client.ts:143

**Parameters:**

▪ **cb**: *function*

▸ (`attemptNumber`: number): *void*

**Parameters:**

Name | Type |
------ | ------ |
`attemptNumber` | number |

**Returns:** *[PublisherKitClient](publisherkitclient.md)*

___

### `Static` updateAccessParams

▸ **updateAccessParams**(`params`: object): *void*

Defined in publisher-kit-client.ts:73

**Parameters:**

▪ **params**: *object*

Name | Type |
------ | ------ |
`token` | string |
`userId` | string |

**Returns:** *void*
