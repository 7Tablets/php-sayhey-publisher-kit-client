module.exports = {
  transform: {
    '^.+\\.ts$': 'ts-jest',
  },
  testPathIgnorePatterns: ['/node_modules/', '__tests__/helper', '__tests__/setup'],
  collectCoverageFrom: ['src/**/*'],
  setupFiles: ['<rootDir>/__tests__/setup/index.ts'],
  globalSetup: '<rootDir>/__tests__/setup/globalSetup.ts',
  globalTeardown: '<rootDir>/__tests__/setup/globalTeardown.ts',
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
  coverageDirectory: 'coverage',
  coverageReporters: ['json', 'lcov', 'text', 'clover'],
  testRegex: '(__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$',
};
