"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var socket_io_client_1 = __importDefault(require("socket.io-client"));
var PublisherKitClient = /** @class */ (function () {
    function PublisherKitClient() {
    }
    // Get the url used to connect web socket to
    PublisherKitClient._socketUrl = function () {
        return PublisherKitClient._publisherUrl + "/" + PublisherKitClient._appId;
    };
    // static publisher url (protocal and port inclusive)
    PublisherKitClient._publisherUrl = 'http://localhost:3001';
    PublisherKitClient._initialized = false;
    PublisherKitClient._subscribed = false;
    PublisherKitClient._isConnected = false;
    PublisherKitClient.initialize = function (options) {
        if (!PublisherKitClient._initialized) {
            var appId = options.appId, appToken = options.appToken, clientApiKey = options.clientApiKey, publisherUrl = options.publisherUrl, userId = options.userId;
            if (!appId || !clientApiKey) {
                return false;
            }
            PublisherKitClient._appId = appId;
            PublisherKitClient._clientApiKey = clientApiKey;
            if (userId) {
                PublisherKitClient._userId = userId;
            }
            if (appToken) {
                PublisherKitClient._appToken = appToken;
            }
            PublisherKitClient._publisherUrl = publisherUrl || PublisherKitClient._publisherUrl;
            PublisherKitClient._initialized = true;
        }
        return PublisherKitClient._initialized;
    };
    PublisherKitClient.updateAccessParams = function (params) {
        var token = params.token, userId = params.userId;
        PublisherKitClient._appToken = token;
        PublisherKitClient._userId = userId;
    };
    PublisherKitClient.connect = function () {
        if (PublisherKitClient._initialized && PublisherKitClient._subscribed) {
            if (PublisherKitClient._isConnected) {
                PublisherKitClient._socket.disconnect();
            }
            PublisherKitClient._socket = socket_io_client_1.default(PublisherKitClient._socketUrl(), {
                autoConnect: false,
                forceNew: false,
                transports: ['websocket'],
                upgrade: false,
                query: {
                    token: PublisherKitClient._appToken,
                    appId: PublisherKitClient._appId,
                    clientApiKey: PublisherKitClient._clientApiKey,
                    userId: PublisherKitClient._userId,
                },
            });
            PublisherKitClient._socket
                .on('connect', function () {
                var _a, _b;
                (_b = (_a = PublisherKitClient)._connectCallback) === null || _b === void 0 ? void 0 : _b.call(_a);
                PublisherKitClient._isConnected = true;
            })
                .on('disconnect', function (reason) {
                var _a, _b;
                (_b = (_a = PublisherKitClient)._disconnectCallback) === null || _b === void 0 ? void 0 : _b.call(_a, reason);
                PublisherKitClient._isConnected = false;
            })
                .on('error', function (error) {
                var _a, _b;
                (_b = (_a = PublisherKitClient)._errorCallback) === null || _b === void 0 ? void 0 : _b.call(_a, error);
            })
                .on('reconnect', function (attemptNumber) {
                var _a, _b;
                (_b = (_a = PublisherKitClient)._reconnectCallback) === null || _b === void 0 ? void 0 : _b.call(_a, attemptNumber);
            })
                .on('message', function (message) {
                PublisherKitClient._messageCallback(message);
            });
            PublisherKitClient._socket.connect();
        }
        else {
            throw new Error('Please check your initialization parameters');
        }
    };
    PublisherKitClient.isConnected = function () { return PublisherKitClient._isConnected; };
    PublisherKitClient.disconnect = function () {
        if (PublisherKitClient._socket && PublisherKitClient._socket.connected) {
            PublisherKitClient._socket.disconnect();
        }
    };
    PublisherKitClient.onConnect = function (cb) {
        PublisherKitClient._connectCallback = cb;
        return PublisherKitClient;
    };
    PublisherKitClient.onDisconnect = function (cb) {
        PublisherKitClient._disconnectCallback = cb;
        return PublisherKitClient;
    };
    PublisherKitClient.onError = function (cb) {
        PublisherKitClient._errorCallback = cb;
        return PublisherKitClient;
    };
    PublisherKitClient.onReconnect = function (cb) {
        PublisherKitClient._reconnectCallback = cb;
        return PublisherKitClient;
    };
    PublisherKitClient.onMessage = function (cb) {
        PublisherKitClient._messageCallback = cb;
        PublisherKitClient._subscribed = true;
        return PublisherKitClient;
    };
    return PublisherKitClient;
}());
exports.default = PublisherKitClient;
//# sourceMappingURL=publisher-kit-client.js.map