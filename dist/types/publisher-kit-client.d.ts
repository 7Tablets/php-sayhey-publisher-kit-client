import { Socket } from 'socket.io-client';
import { EventMessage, InitOptions } from './types';
export * from './types';
export default class PublisherKitClient {
    static _appToken: string;
    static _appId: string;
    static _clientApiKey: string;
    static _publisherUrl: string;
    static _socket: typeof Socket;
    static _connectCallback?: () => void;
    static _disconnectCallback?: (reason: string) => void;
    static _errorCallback?: (error: Error) => void;
    static _reconnectCallback?: (attemptNumber: number) => void;
    static _messageCallback: (message: EventMessage) => void;
    static _initialized: boolean;
    static _subscribed: boolean;
    static _isConnected: boolean;
    static _userId: string;
    static _platform: 'web' | 'ios' | 'android';
    static _socketUrl(): string;
    static initialize: (options: InitOptions) => boolean;
    static updateAccessParams: (params: {
        token: string;
        userId: string;
    }) => void;
    static connect: () => void;
    static isConnected: () => boolean;
    static disconnect: () => void;
    static onConnect: (cb: () => void) => typeof PublisherKitClient;
    static onDisconnect: (cb: (reason: string) => void) => typeof PublisherKitClient;
    static onError: (cb: (e: Error) => void) => typeof PublisherKitClient;
    static onReconnect: (cb: (attemptNumber: number) => void) => typeof PublisherKitClient;
    static onMessage: (cb: (message: EventMessage) => void) => typeof PublisherKitClient;
}
