export interface InitOptions {
    appToken?: string;
    appId: string;
    clientApiKey: string;
    publisherUrl?: string;
    userId?: string;
}
export interface EventMessage {
    event: string;
    payload: any;
}
