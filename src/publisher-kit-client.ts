import io, { Socket } from 'socket.io-client';

import { EventMessage, InitOptions } from './types';

export * from './types';

export default class PublisherKitClient {
  // Token publisher will pass to authentication endpoint.
  static _appToken: string;

  // ID of the client app registered in publisher.
  static _appId: string;

  // API key of the client app registered in publisher.
  static _clientApiKey: string;

  // static publisher url (protocal and port inclusive)
  static _publisherUrl = 'http://localhost:3001';

  // The web socket used to communicate with publisher
  static _socket: typeof Socket;

  // Callback to be called when connection event emitted
  static _connectCallback?: () => void;

  // Callback to be called when disconnect event emitted
  static _disconnectCallback?: (reason: string) => void;

  // Callback to be called when an error occur
  static _errorCallback?: (error: Error) => void;

  // Callback to be called when reconnect event emitted
  static _reconnectCallback?: (attemptNumber: number) => void;

  // Callback to be called when message event emitted
  static _messageCallback: (message: EventMessage) => void;

  static _initialized = false;

  static _subscribed = false;

  static _isConnected = false;

  static _userId: string;

  static _platform: 'web' | 'ios' | 'android';

  // Get the url used to connect web socket to
  static _socketUrl() {
    return `${PublisherKitClient._publisherUrl}/${PublisherKitClient._appId}`;
  }

  static initialize = (options: InitOptions) => {
    if (!PublisherKitClient._initialized) {
      const { appId, appToken, clientApiKey, publisherUrl, userId } = options;
      if (!appId || !clientApiKey) {
        return false;
      }
      PublisherKitClient._appId = appId;
      PublisherKitClient._clientApiKey = clientApiKey;
      if (userId) {
        PublisherKitClient._userId = userId;
      }
      if (appToken) {
        PublisherKitClient._appToken = appToken;
      }
      PublisherKitClient._publisherUrl = publisherUrl || PublisherKitClient._publisherUrl;
      PublisherKitClient._initialized = true;
    }
    return PublisherKitClient._initialized;
  };

  static updateAccessParams = (params: { token: string; userId: string }) => {
    const { token, userId } = params;
    PublisherKitClient._appToken = token;
    PublisherKitClient._userId = userId;
  };

  static connect = () => {
    if (PublisherKitClient._initialized && PublisherKitClient._subscribed) {
      if (PublisherKitClient._isConnected) {
        PublisherKitClient._socket.disconnect();
      }
      PublisherKitClient._socket = io(PublisherKitClient._socketUrl(), {
        autoConnect: false,
        forceNew: false,
        transports: ['websocket'],
        upgrade: false,
        query: {
          token: PublisherKitClient._appToken,
          appId: PublisherKitClient._appId,
          clientApiKey: PublisherKitClient._clientApiKey,
          userId: PublisherKitClient._userId,
        },
      });
      PublisherKitClient._socket
        .on('connect', () => {
          PublisherKitClient._connectCallback?.();
          PublisherKitClient._isConnected = true;
        })
        .on('disconnect', (reason: string) => {
          PublisherKitClient._disconnectCallback?.(reason);
          PublisherKitClient._isConnected = false;
        })
        .on('error', (error: Error) => {
          PublisherKitClient._errorCallback?.(error);
        })
        .on('reconnect', (attemptNumber: number) => {
          PublisherKitClient._reconnectCallback?.(attemptNumber);
        })
        .on('message', (message: EventMessage) => {
          PublisherKitClient._messageCallback(message);
        });
      PublisherKitClient._socket.connect();
    } else {
      throw new Error('Please check your initialization parameters');
    }
  };

  static isConnected = () => PublisherKitClient._isConnected;

  static disconnect = () => {
    if (PublisherKitClient._socket && PublisherKitClient._socket.connected) {
      PublisherKitClient._socket.disconnect();
    }
  };

  static onConnect = (cb: () => void) => {
    PublisherKitClient._connectCallback = cb;
    return PublisherKitClient;
  };

  static onDisconnect = (cb: (reason: string) => void) => {
    PublisherKitClient._disconnectCallback = cb;
    return PublisherKitClient;
  };

  static onError = (cb: (e: Error) => void) => {
    PublisherKitClient._errorCallback = cb;
    return PublisherKitClient;
  };

  static onReconnect = (cb: (attemptNumber: number) => void) => {
    PublisherKitClient._reconnectCallback = cb;
    return PublisherKitClient;
  };

  static onMessage = (cb: (message: EventMessage) => void) => {
    PublisherKitClient._messageCallback = cb;
    PublisherKitClient._subscribed = true;
    return PublisherKitClient;
  };
}
